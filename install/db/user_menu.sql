-- phpMyAdmin SQL Dump
-- version 4.3.4
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 03 2015 г., 03:49
-- Версия сервера: 5.6.22-log
-- Версия PHP: 5.5.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test_local`
--

-- --------------------------------------------------------

--
-- Структура таблицы `user_menu`
--

CREATE TABLE IF NOT EXISTS `user_menu` (
  `id` int(11) NOT NULL,
  `type` enum('link','razd','inc') NOT NULL DEFAULT 'link',
  `name` varchar(32) NOT NULL,
  `type_set` varchar(32) NOT NULL,
  `url` varchar(32) NOT NULL,
  `counter` varchar(32) NOT NULL,
  `pos` int(11) NOT NULL,
  `icon` varchar(32) DEFAULT NULL,
  `pos2` int(11) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='Кабинет';

--
-- Дамп данных таблицы `user_menu`
--

INSERT DELAYED INTO `user_menu` (`id`, `type`, `name`, `type_set`, `url`, `counter`, `pos`, `icon`, `pos2`) VALUES
(11, 'inc', 'Установки аватара и смена тем', 'settings', 'ava_them.php', '', 3, 'default.png', 2),
(8, 'inc', 'Правила и информация', 'index', 'info_r.php', '', 6, 'default.png', 0),
(7, 'inc', 'Почта и журнал', 'index', 'm_j.php', '', 4, 'default.png', 0),
(4, 'inc', 'Стартовая страница', 'index', 'start_page.php', '', 2, 'default.png', 0),
(2, 'inc', 'Пользователи', 'index', 'users.php', '', 3, 'default.png', 0),
(3, 'inc', 'Анкета и редактирование', 'index', 'profile.php', '', 1, 'default.png', 0),
(1, 'inc', 'Пополнения счета', 'settings', 'payment.php', '', 5, 'default.png', 4),
(10, 'inc', 'Смена пароля и языка', 'settings', 'set_l_p.php', '', 4, 'default.png', 1),
(9, 'inc', 'Выход', 'index', 'exit_set.php', '', 8, 'default.png', 0),
(6, 'inc', 'Настойки и история авторизаций ', 'index', 'set_hist.php', '', 7, 'default.png', 0),
(5, 'inc', 'Приложения', 'index', 'apps.php', '', 5, 'default.png', 0),
(12, 'inc', 'Доп. настройки и установка ссылк', 'settings', 'dop_set_link.php', '', 0, 'default.png', 3),
(15, 'inc', 'email и телефон', 'settings', 'email_phone.php', '', 9, NULL, 6),
(16, 'inc', 'Админ панель', 'index', 'apanel.php', '', 9, 'default.png', 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`), ADD KEY `pos` (`pos`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
